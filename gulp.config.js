module.exports = {
  app: { baseName: 'app' },
  sass: {
      src:         ['app/styles/*.scss'],
      outputStyle: 'compressed'
  },
  prodFolder: {
      css: './minified',
      js:  './minified'
  },
  devFolder: {
    css: './app/styles/',
    js:  './app/scripts'
  },
  isDev: true
}

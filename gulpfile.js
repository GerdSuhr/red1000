// generated on 2017-04-21 using generator-webapp 2.4.1
const gulp = require('gulp');
const gulpLoadPlugins = require('gulp-load-plugins');
const browserSync = require('browser-sync').create();
const del = require('del');
const wiredep = require('wiredep').stream;
const browserify = require('browserify');
const babelify = require('babelify');
const buffer = require('vinyl-buffer');
const uglify = require('gulp-uglify');
const source = require('vinyl-source-stream');
const runSequence = require('run-sequence');
const sassInlineImage = require('sass-inline-image');
const merge = require('merge-stream');
const CONFIGS = [require('./gulp.config')];
const styleInject = require('gulp-style-inject');
//const inject = require('gulp-inject');
const $ = gulpLoadPlugins();
const reload = browserSync.reload;

let env = 'dev';

// CLEAN
gulp.task('clean', del.bind(null, ['minified','.tmp','dist']));

// STYLES
gulp.task('styles', () => {
  let tasks = CONFIGS.map(config => {
    return gulp.src(config.sass.src)
        .pipe($.plumber())
        .pipe($.if(env == 'dev', $.sourcemaps.init()))
        .pipe($.sass.sync({
          outputStyle: config.sass.outputStyle,
          precision: 10,
          includePaths: ['.'],
          functions: sassInlineImage({ /* options */ })
        }).on('error', $.sass.logError))
        .pipe($.autoprefixer({browsers: ['> 1%', 'last 2 versions', 'Firefox ESR']}))
        .pipe($.if(env == 'dev', $.sourcemaps.write()))
        .pipe($.if(env == 'dev', gulp.dest(config.devFolder.css), gulp.dest('./minified')) )
    .pipe(reload({stream: true}));
  });

  return merge(tasks);
});

// BUNDLE
const createBundle = options => {
    const b = browserify({
      entries: 'app/scripts/main.js',
      transform: babelify,
      debug: true
    });
    const rebundle = () =>
      b.bundle()
        .pipe(source('red1000.min.js'))
        .pipe($.plumber())
        .pipe(buffer())
        //.pipe($.if(env == 'prod',uglify()))
        .pipe($.sourcemaps.init({loadMaps: true}))
        .pipe($.sourcemaps.write('.'))
        .pipe($.sourcemaps.write('../maps'))
        .pipe(gulp.dest(options.destination))
        .pipe(reload({stream: true}));
    b.on('update', rebundle);
    return rebundle();
};

// SCRIPTS DEV
gulp.task('scripts:dev',['set-dev'], () => {
    const b = browserify({
      entries: 'app/scripts/main.js',
      transform: babelify,
      debug: true
    });
    CONFIGS.forEach(config => createBundle({
      destination: config.devFolder.js
    }));
});

// SCRIPTS PRODUCTION
gulp.task('scripts:prod',['set-prod'], () => {
    const b = browserify({
      entries: 'app/scripts/main.js',
      transform: babelify,
      debug: true
    });
    CONFIGS.forEach(config => createBundle({
      destination: config.prodFolder.js
    }));
    return gulp.src('./maps')
      .pipe(gulp.dest('./maps'));
});


function lint(files) {
  return gulp.src(files)
    .pipe($.eslint({ fix: true }))
    .pipe(reload({stream: true, once: true}))
    .pipe($.eslint.format())
    .pipe($.if(!browserSync.active, $.eslint.failAfterError()));
}

gulp.task('lint', () => {
  return lint('app/scripts/**/*.js')
    .pipe(gulp.dest('app/scripts'));
});

// gulp.task('lint:test', () => {
//   return lint('test/spec/**/*.js')
//     .pipe(gulp.dest('test/spec'));
// });

// creates index,html and messy.html in dist folder
gulp.task('html', () => {
  return gulp.src('app/*.html')
    .pipe($.useref({searchPath: ['.tmp', 'app', '.']}))
    //.pipe($.if(/\.js$/, $.uglify({compress: {drop_console: true}})))
    .pipe($.if(/\.css$/, $.cssnano({safe: true, autoprefixer: false})))
    .pipe($.if(/\.html$/, $.htmlmin({
      collapseWhitespace: false,
      //minifyCSS: true,
      //minifyJS: {compress: {drop_console: true}},
      processConditionalComments: false,
      buildremoveComments: false,
      removeEmptyAttributes: true,
      removeScriptTypeAttributes: true,
      removeStyleLinkTypeAttributes: true
    })))
    .pipe(gulp.dest('./minified'));
});

// gulp.task('images', () => {
//   return gulp.src('app/images/**/*')
//     .pipe($.cache($.imagemin()))
//     .pipe(gulp.dest('dist/images'));
// });


// favicon, robots.txt, apple-touch
// gulp.task('extras', () => {
//   return gulp.src([
//     'app/*',
//     '!app/*.html'
//   ], {
//     dot: true
//   }).pipe(gulp.dest('dist'));
// });

gulp.task('fonts', () => {
  return gulp.src(require('main-bower-files')('**/*.{eot,svg,ttf,woff,woff2}', function (err) {})
    .concat('app/fonts/**/*'))
    .pipe($.if(env == 'dev', gulp.dest('.tmp/fonts'), gulp.dest('dist/fonts')));
});

gulp.task('serve', () => {
  runSequence(['clean'], ['set-dev', 'styles', 'scripts:dev', 'fonts'], () => {
    browserSync.init({
      notify: false,
      port: 9000,
      server: {
        baseDir: ['.tmp', 'app'],
        routes: {
          '/bower_components': 'bower_components'
        }
      }
    });

    gulp.watch([
      'app/*.html',
      'app/images/**/*',
      '.tmp/fonts/**/*'
    ]).on('change', reload);

    gulp.watch('app/styles/**/*.scss', ['styles']);
    gulp.watch('app/scripts/**/*.js', ['scripts:dev']);
    gulp.watch('app/fonts/**/*', ['fonts']);
    gulp.watch('bower.json', ['wiredep', 'fonts']);
  });
});

// gulp.task('serve:dist', ['default'], () => {
//   browserSync.init({
//     notify: false,
//     port: 9000,
//     ghostMode: {
//       clicks: true,
//       forms: true,
//       scroll: true
//     },
//     server: {
//       baseDir: ['dist']
//     }
//   });
// });

// gulp.task('serve:test', ['scripts'], () => {
//   browserSync.init({
//     notify: false,
//     port: 9000,
//     ui: false,
//     server: {
//       baseDir: 'test',
//       routes: {
//         '/scripts': '.tmp/scripts',
//         '/bower_components': 'bower_components'
//       }
//     }
//   });

//   gulp.watch('app/scripts/**/*.js', ['scripts']);
//   gulp.watch(['test/spec/**/*.js', 'test/index.html']).on('change', reload);
//   gulp.watch('test/spec/**/*.js', ['lint:test']);
// });

// inject bower components
gulp.task('wiredep', () => {
  gulp.src('app/styles/*.scss')
    .pipe($.filter(file => file.stat && file.stat.size))
    .pipe(wiredep({
      ignorePath: /^(\.\.\/)+/
    }))
    .pipe(gulp.dest('app/styles'));

  gulp.src('app/*.html')
    .pipe(wiredep({
      exclude: ['bootstrap-sass'],
      ignorePath: /^(\.\.\/)*\.\./
    }))
    .pipe(gulp.dest('app'));
});



gulp.task('set-dev', function() {
  return env = 'dev';
});

gulp.task('set-prod', function() {
  return env = 'prod';
});


//gulp.task('dev', ['set-dev','clean', 'html']);
gulp.task('minify', ['scripts:prod'], function() {
  runSequence('clean', 'set-prod', 'styles', 'html');
});


gulp.task('inject', function () {
  return gulp.src('./minified/production.html')
    .pipe(styleInject({
      encapsulated: false,
      //path: './app/styles/main.css',
    }))
    .pipe(gulp.dest('./dist'));
});


// gulp.task('build',['scripts:prod'], function() {
//   runSequence('clean', 'set-prod', 'styles', 'html', 'inject');
// });

// gulp.task('build', () => {
//   return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
// });

// gulp.task('build', ['lint', 'html', 'images', 'fonts', 'extras'], () => {
//   return gulp.src('dist/**/*').pipe($.size({title: 'build', gzip: true}));
// });

// gulp.task('default', () => {
//   return new Promise(resolve => {
//     dev = false;
//     runSequence(['clean', 'wiredep'], 'build', resolve);
//   });
// });

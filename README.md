# Red1000

## Setup as of 4/2022##

Make sure you are on the "working-2022" branch! Run npm install.
Important Note:  
 "overrides": {"graceful-fs": "^4.2.9"} in package.json allows gulp3.9 to run on newer versions(9+) of node.js
see: https://stackoverflow.com/questions/55921442/how-to-fix-referenceerror-primordials-is-not-defined-in-node-js

$ npm install

## To run the application in development mode.

$ gulp serve

To build the single page script that can be inserted in AEM script component.
$ gulp minify
$ gulp inject

## How the single page script gets built. 11/2018

The file named production.html in the app folder is a duplicate of the index.html in the app folder.
Tht production.html file has the comments in the head and end of body where the minified CSS and JS get injected.
If you make a change to index.html you will need to make those changes in production.html as well.
Just remember you need to have the comments in production.html so that the CSS and JS gets injected.

$ gulp minify (runs the tasks to minify and move needed files to the 'minified' folder)
$ gulp inject (runs the task to inject the minified CSS and JS and saves it to the 'dist' folder)

This will create the file production.html in dist folder. This is the file that gets placed in the script component on AEM.

## Background (old deprecated stuff from 2017) left as reference

The project is built using [Yeoman](http://yeoman.io/) [webapp generator](https://github.com/yeoman/generator-webapp)

```shell
# To Install the necessary tools
$ npm install --global yo gulp-cli bower generator-webapp
# To build the webapp
$ yo webapp
```

During the **yo** process the Sass feature was selected, jQuery was included, and BDD was selected for the Domain-Specific Language option.

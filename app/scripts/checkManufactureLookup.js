//import './jqueryGlobal';
//import typeahead from 'typeahead.js-jspm';
//import {Bloodhound} from 'typeahead.js-jspm';

export default function checkManufactureLookup( event, option1 ) {

    $('#option2').typeahead('destroy');
    $('.red1000__product').removeClass('active');
    $(event).addClass('active');

    $('#option2').attr('placeholder', 'Enter part number');

    $('#otherForm').hide();
    $('.input-group').show();
    $('#ssform').show();


    // Defining the local dataset
    let listItemsL = [];
    let listItemsL2 = [];
    document.getElementById('option2').value = '';


    let letManufactureL = option1;
    document.getElementById('option1').value = option1;
    let asManL= $.unique( $(json).filter(function (i,n){return n.Man.toLowerCase() === letManufactureL.toLowerCase() }) );

    for (let i=0;i<asManL.length;i++) {
        let option = document.createElement('option');
        listItemsL.push(asManL[i].Part);
    }

    listItemsL2 = $.unique( listItemsL ).sort();

    let cars = listItemsL2;


    // on enter press, do part search
    $('#option2').keypress(function (e) {
      // force to uppercase. All part numbers are uppercase
      var value = $(this).val().toUpperCase()
      // did the user enter a numeric value, if so convert it from a string to a number.
      $.isNumeric(value) ? value = Number(value) : ''
      //console.log($.isNumeric(value));
      var key = e.which;
      if(key == 13) {
        checkProducts(value)
       }
     });

    // ++++++++++++++ Code to check the whole string

    let substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            let matches, substringRegex;

            // an array that will be populated with substring matches
            matches = [];

            // regex used to determine if a string contains the substring `q`
            let substrRegex = new RegExp(q, 'i');

            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });

            cb(matches);
      };
    };


    // Constructing the suggestion engine
    cars = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.whitespace,
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        local: cars,
        limit: 15
    });

    // Initializing the typeahead
    $('.typeahead').typeahead({
        hint: true,
        highlight: true, /* Enable substring highlighting */
        minLength: 1, /* Specify minimum characters required for showing result */
        limit: 15
    },
    {
        name: 'cars',
        //source: cars,
        source: substringMatcher(listItemsL2),
        limit: 1000
    }).on('typeahead:selected', function (obj, datum) {
        checkProducts(datum);
    });


    $('#option2').focus();
}

function checkProducts(lookupItemNo) {

    $('#divOption2loading').show();
    // $('#divOption2').hide();

    let productitems = [];

    //let product_option1 = document.getElementById("option1").value;
    let product_option1 = 'Parker';
    let product_option2 = lookupItemNo;
    let productItemsShown = 0;

    //console.log(typeof product_option2);

    let sku = $.unique( $(json).filter(function (i,n){return n.Part===product_option2}) );
    //console.dir(sku[0]);

    if (sku.length && sku[0].url && sku[0].url.length) {
      $('.red1000__purchase').css('display', 'flex');
      $('#ssform').show();
      window.onresize();
    } else {
      $('.red1000__purchase').css('display', 'none');
      $('#ssform').show();
      window.onresize();
    }

    for (let i=0; i<sku.length; i++) {
        // document.getElementById("productRed").value = sku['0'].Pall;
        document.getElementById('productItem1').innerHTML = sku[0].Pall;
        document.getElementById('productItem2').innerHTML = sku[0].Per;
        document.getElementById('productItem3').innerHTML = sku[0].Exp;
        document.getElementById('productItem4').innerHTML = sku[0].Notes;
        document.getElementById('producturl').href=sku[0].url;

        let productItems = [
            'productItem1',
            'productItem2',
            'productItem3',
            'productItem4'
        ];

        productItems.map(function(idQuery) {
            let element = $('#' + idQuery);

            if (document.getElementById(idQuery).innerHTML.trim() == '') {
                element.parent().hide();
            } else {
                element.parent().show();
                productItemsShown ++;
            }
        });
    }
    $('form').removeClass('disable');
    $('form').css('color','#fff');

    $('#thankyouNowDiv').hide();
    $('#completeStepOne').hide();
    $('#otherReplacement').hide();
    $('#describeReplacement').show();
    sku.length === 0 ? $('#specs').css('display', 'none') : $('#specs').css('display', 'block')

    $('.red1000__step-two-banner').removeClass('show-replacement-1');
    $('.red1000__step-two-banner').removeClass('show-replacement-2');
    $('.red1000__step-two-banner').removeClass('show-replacement-3');
    $('.red1000__step-two-banner').removeClass('show-replacement-4');
    $('.red1000__step-two-banner').addClass('show-replacement-' + productItemsShown);
    $('#divOption2loading').hide();
    $('#divOption2').show();

    $('#ssformTitle').hide();
    $('#ssformThankYou').hide();
    $('#otherForm').hide();
    window.onresize();
}


//import './jqueryGlobal';
//import typeahead from 'typeahead.js-jspm';

//import {Bloodhound} from 'typeahead.js-jspm';
import checkManufactureLookup from './checkManufactureLookup.js';
import formConnector from './formConnector.js';
//typeahead.loadjQueryPlugin();


function contactNOW(event) {
    $('.red1000__product').removeClass('active');
    $('#otherForm label').css('color', 'white');
    $('.red1000__purchase').hide();
    $('.input-group').hide();
    $(event).addClass('active');
    $('#completeStepOne').hide();
    $('#describeReplacement').hide();
    $('#otherReplacement').show();
    $('#hiddenClick').hide();
    $('#otherForm').show();
    $('#ssform').hide();
    $('#ssformTitle').hide();
    $('.mktoFormRow.width-full textarea').focus();
    window.onresize();
}

function thankyouDiv() {
    $('#otherForm').hide();
    $('#ssform').hide();
    $('#thankyouNowDiv').show();
    window.onresize();
}

function replaceSubmitButton() {
    $('.mktoFieldWrap:has(textarea)').addClass('height-auto');
    $('.mktoFormRow:has(textarea)').addClass('width-full');
    $('#mktoForm_91 .mktoButton').addClass('btn form-connector__submit-button');
    $('#mktoForm_135 .mktoButton').addClass('btn form-connector__submit-button');
    $('#mktoForm_91 .mktoButton').html('<div><span class="icon-checkmark"></span><span><span>Submit<span>Get a Quote</span></span></span></div>');
    $('#mktoForm_91 .mktoButton').prop('disabled', false);
    $('#mktoForm_135 .mktoButton').html('<div><span class="icon-checkmark"></span><span><span>Submit<span>Get a Quote</span></span></span></div>');
    $('#mktoForm_135 .mktoButton').prop('disabled', false);
    if ($('#mktoForm_91').has('.clear').length == 0) {
        let interval91 = setInterval(() => {
            if ($('#mktoForm_91').html().trim() != '') {
                $('#mktoForm_91').append('<div class="clear"></div>');
                $('.mktoCheckboxList').parent().find('label.mktoLabel.mktoHasWidth').addClass('privacy-policy');
                clearInterval(interval91);
            }
        }, 100);
    }

    if ($('#mktoForm_135').has('.clear').length == 0) {
        let interval135 = setInterval(() => {
            if ($('#mktoForm_135').html().trim() != '') {
                $('#mktoForm_135').append('<div class="clear"></div>');
                clearInterval(interval135);
            }
        }, 100);
    }

    // document.querySelector('.privacy-policy').style = "padding-left: 30px !important";
    if (document.querySelector('.mktoFormRow.width-full') !== null)
    document.querySelector('.mktoFormRow.width-full').style = 'padding-right: 0px !important';
}

function resizeStepOne() {
  setTimeout(function(){
    if (window.innerWidth > 991) {
        let minimumHeight = $('.red1000__step-two').height();
        $('.red1000__step-one').css('min-height', (minimumHeight + 20) + 'px');
    } else {
        $('.red1000__step-one').css('min-height', '0px');
    }
  }, 300);
}

function initResizeStepOne() {
    window.onresize = () => resizeStepOne()
}

initResizeStepOne();

window.RED = {
    checkManufactureLookup: checkManufactureLookup,
    contactNOW: contactNOW,
    thankyouDiv: thankyouDiv,
    formConnector: formConnector,
    replaceSubmitButton: replaceSubmitButton,
    resizeStepOne: resizeStepOne,
    init: () => {
        $('#start-again').click(function() {
          location.reload(true);
            // $('#otherForm').hide();
            // $('#ssform').show();
            // $('#ssform form').addClass('disable');
            //$('#thankyouNowDiv').hide();
        });
    }
}

//import './jqueryGlobal';
export default {

    init: function() {
        this.setUp();

        this.setSelectedLanguageOnLinks(this.getSelectedLanguage());

        $('.download-language--gated').unbind('click').bind('click', function(e) {
            var $link = $(e.currentTarget);
            var $form = $link.parents('.form-connector__download-links-wrapper').siblings('.form-connector--download');
            if (!$form.is('form') || this.validateForm($form)) {
                this.makeDownloadPostToMarketo($form, $link, e);
            } else {
                e.preventDefault();
            }
        });
    },

    _validatedForms: [],

    clearDownloadLinks: function() {
        $('.download-language.active').removeClass('active');
    },

    setSelectedLanguage: function(lang) {
        if (window.localStorage !== null) {
            localStorage['pallSelectedDownloadLanguage'] = lang;
        }
    },

    getSelectedLanguage: function() {
        return window.localStorage !== null
            ? localStorage['pallSelectedDownloadLanguage']
            : null;
    },

    activeLanguageLink: function($link) {
        $link.addClass('active');
    },

    setSelectedLanguageOnLinks: function(selectedLang) {
        selectedLang = selectedLang != null
            ? selectedLang
            : 'en';
        $('.form-connector--download__languages').each(function() {
            var $this = $(this);
            var changed = false;
            $this.find('.download-language').each(function() {
                var lang = $(this).data('lang');
                if (lang == selectedLang) {
                    this.activeLanguageLink($(this));
                    changed = true;
                }
            });
            if (!changed) {
                this.activeLanguageLink($this.find('.download-language--en'));
            }
        });
    },

    validateDropdown: function($dropdown) {
        var $value = $dropdown.find('.form-connector__select__item');
        if ($value.is(':empty')) {
            $dropdown.addClass('invalid');
            return false;
        } else {
            $dropdown.removeClass('invalid');
            return true;
        }
    },

    validateForm: function($form) {
        var formId = $form.find('input[name="formid"]').val();

        if ($.inArray(formId, this._validatedForms) > -1) {
            return true;
        }

        $form.removeAttr('novalidate');
        $form.find('input:not(:hidden)').addClass('validate');

        var valid = true;
        $form.find('.form-connector__select:not(.novalidate)').each(function() {
            valid = this.validateDropdown($(this)) && valid;
        });

        if ($form[0].checkValidity() && valid) {
            this._validatedForms.push(formId);
            return true;
        }
        return false;
    },

    hideFormsOnCurrentPage: function($form) {
        var formId = $form.find('input[name="formid"]').val();
        var valid = true;
        $form.find('.form-connector__select:not(.novalidate)').each(function() {
            valid = this.validateDropdown($(this)) && valid;
        });
        if (valid) {
            $('input[name="formid"]').each(function() {
                var $this = $(this);
                if ($this.val() == formId) {
                    $this.closest('form').hide();
                }
            });
        }
    },

    clearEvents: function() {
        var self = this;
        var $formFields = $('.form-connector__field, .mktoField');
        $formFields.off('checkval');
        $formFields.off('keyup', self.keyupHandler).off('focus').off('blur');
        $('.form-connector__input-container__label, .mktoLabel').off();
        $('.form-connector__select').off('click', self.selectItemHandler);
        $('.form-connector__select__menu__option__item').each(function() {
            $(this).off('click', self.dropdownItemClickHandler);
        });
        $('.form-connector__submit-button').off();
        $('body').off('click', self.hideExpandedDropdowns);
    },

    selectItemHandler: function(e) {
        e.stopPropagation();
        $(e.currentTarget).siblings('.form-connector__select__menu').toggleClass('hide-menu');
    },

    keyupHandler: function() {
        $(this).trigger('checkval');
    },

    hideExpandedDropdowns: function(e) {
        $('.form-connector__select__menu').addClass('hide-menu');
    },

    setUp: function() {
        var self = this;
        this.clearEvents();

        var onClass = 'on';
        var showClass = 'shown disable';

        $('.form-connector__field, .mktoField').bind('checkval', function() {
            var label = $(this).siblings('label');

            if (this.value !== '') {
                label.addClass(showClass);
            } else {
                label.removeClass(showClass);
            }
        }).on('keyup', self.keyupHandler).on('focus', function() {
            $(this).siblings('label').addClass(onClass);
        }).on('blur', function() {
            $(this).siblings('label').removeClass(onClass);
        }).trigger('checkval');

        $('.form-connector__input-container__label, .mktoLabel').on('click', function() {
            var $input = $(this).siblings('input');
            $input.focus();
        });

        $('.form-connector__select').on('click', self.selectItemHandler);

        $('.form-connector__select__menu__option__item').each(function() {
            $(this).on('click', self.dropdownItemClickHandler);
        });

        /**
         * When the submit button is activated attempt to validate the form.
         */
        $('.form-connector__submit-button').on('click', function(e) {
            var $button = $(e.currentTarget);
            var $form = $button.parents('form');
            this.validateForm($form);
        });

        //hide expanded dropdowns
        $('body').on('click', self.hideExpandedDropdowns);
    },

    dropdownItemClickHandler: function(e) {
        var $this = $(e.currentTarget);
        var $menu = $this.parents('.form-connector__select__menu');
        var $select = $menu.siblings('.form-connector__select');
        $this.removeClass('active');
        $menu.toggleClass('hide-menu');
        $select.find('.form-connector__select__title').addClass('hide');
        $this.addClass('active');
        var value = $(this).html();
        $select.find('.form-connector__select__item').html(value);
        $select.find('input:hidden').first().val(value);
    },

    // setCustomSubmit: function($submitButton, callback) {
    //     $submitButton.unbind('click').bind('click', callback);
    // },

    makePostToMarketo: function($form) {
        return $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            data: $form.serialize(),
            success: function(data) {
                this.hideFormsOnCurrentPage($form);
                var $returnUrl = $form.find('input[name=returnURL]');
                if ($returnUrl.length !== 0) {
                    window.location = $returnUrl.val();
                } else {
                    this.displayThankYouMessage($form)
                }
            }
        });
    },

    makeDownloadPostToMarketo: function($form, $link, event) {
        $.ajax({
            type: 'POST',
            url: $form.attr('action'),
            async: false,
            data: $form.serialize(),
            success: function(data) {
                this.hideFormsOnCurrentPage($form);
                this.clearDownloadLinks();
                this.setSelectedLanguage($link.data('lang'));
                this.setSelectedLanguageOnLinks($link.data('lang'));
                this.activeLanguageLink($link);
                var $returnUrl = $form.find('input[name=returnURL]');
                if ($returnUrl.length !== 0) {
                    window.location = $returnUrl.val();
                } else {
                    this.displayThankYouMessage($form);
                }
            },
            error: function(request, status, error) {
                event.preventDefault();
            }
        });
    },

    displayThankYouMessage: function($form) {
        var $thankyouMessage = $form.siblings('.form-connector__thankyou-message');
        $form.hide();
        $thankyouMessage.show();
    },

    needToInit: function() {
        return $('.form-connector').length > 0 || $('.search-results-component').length > 0;
    }
};
